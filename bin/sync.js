import fs from 'fs';
import axios from 'axios';
import APIS from './../src/api.json';

const Prefix = 'http://47.114.47.71:6010';

/**
 * pc: parentCode
 * pn: parentName
 * sn: secondLevelName
 * rc: rootCode
 * rn: rootName
 * tn: threeLevelName
 * p: pinyin
 * i: initial
 */
let importStr = '';
let exportStr = 'export {';
APIS.map(function (item, index) {
  importStr += "import " + item.name + " from './" + item.name + ".json';\n";
  exportStr += item.name +', '
  axios.get(Prefix + item.api).then(function (resp) {
    let data = JSON.stringify(resp.data.list);
    if (item.name != 'school') { //学校字典不需要修改
      data = data.replace(/parentCode/g,'pc')
        .replace(/parentName/g,'pn')
        .replace(/secondLevelName/g,'sn')
        .replace(/rootCode/g,'rc')
        .replace(/rootName/g,'rn')
        .replace(/threeLevelName/g,'tn')
        .replace(/initial/g,'i')
        .replace(/pinyin/g,'p')
        .replace(/code/g,'value')
        .replace(/name/g,'label');
    }

    fs.writeFile('rawData/' + item.name + '.json', data, function (err) {
      if (err) {
        console.info(err);
        throw err;
      }
      console.log('The file ' + (index + 1) + ' ' + item.name + ' has been saved!');
    });
  }).catch(function (err){
    console.error('call api faile:', item.api)
  })
})
exportStr = exportStr.substring(-1, exportStr.length-2)
exportStr += '}'

fs.writeFile('rawData/index.js', importStr + exportStr, function (err) {
  if (err) {
    console.info(err);
    throw err;
  }
  console.log('The file index.js has been saved!');
});
