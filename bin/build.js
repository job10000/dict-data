import fs from 'fs';
import APIS from '../src/api.json';
import * as _Dict from '../rawData';
import complaint from '../extData/complaint.json';
import areaAcronym from '../extData/areaAcronym.json';
import postWorkYearNum from '../extData/postWorkYearNum.json';
import yearOldNum from '../extData/yearOldNum.json';
import hotFlag from '../extData/hotFlag.json'

function formatDict (source) {
  let tmpObj = {}
  let result = []

  //数组处理成临时存储结构
  source.map(function (_item) {
    if (!_item) return;
    let item = Object.assign({}, _item)
    let code = item.value

    if (item.pc === 0) {
      if (!tmpObj[code]) {
        tmpObj[code] = {children: []}
      }
      result.push(item)
    } else if (!item.rc || item.pc === item.rc) {
      let rootCode = item.pc

      //将二级依次保存于root的children
      if (!tmpObj[rootCode]) {
        tmpObj[rootCode] = {children: []}
      }
      tmpObj[rootCode].children.push(item)

      //将二级单独存储，便于把三级放到children
      if (!tmpObj[code]) {
        tmpObj[code] = {children: []}
      }
    } else {
      let secondCode = item.pc
      if (!tmpObj[secondCode]) {
        tmpObj[secondCode] = {children: []}
      }
      tmpObj[secondCode].children.push(item)
    }
  })

  return result.map(function (root) {
    root.children = tmpObj[root.value].children
      .map(function (second) {
        let secondSubData = tmpObj[second.value]
        if (secondSubData.children.length) {
          second.children = secondSubData.children
        } else {
          delete second.children;
        }

        return second
      })

    if (root.children.length == 0) {
      delete root.children
    }

    return root
  })
}

let Dict = Object.assign({}, _Dict)
Dict.areaTree = formatDict(Dict.area)
Dict.jobTypeTree = formatDict(Dict.jobType)
Dict.companyTypeTree = formatDict(Dict.companyType)
Dict.personHonorTree = formatDict(Dict.personHonor)
Dict.teacherTitleTree = formatDict(Dict.teacherTitle)


const workYears = [
  {value: -2, label: '在读学生'},
  {value: -1, label: '应届生'},
  {value: 0, label: '1年以内'}
];

for (var i = 1, l = 50; i <= l; i++) {
  workYears.push({value: i, label: i + '年'})
}

Dict.workYearNum = workYears;

Dict.salary = [
  {label: '3000元以下', value: 10},
  {label: '3000-5000元', value: 20},
  {label: '5000-10000元', value: 30},
  {label: '10000-20000元', value: 40},
  {label: '20000-50000元', value: 50},
  {label: '50000元以上', value: 60}
]

Dict.complaint = complaint
Dict.areaAcronym = areaAcronym
Dict.postWorkYearNum = postWorkYearNum
Dict.yearOldNum = yearOldNum
Dict.hotFlag = hotFlag

let extrs = ['areaTree', 'jobTypeTree', 'companyTypeTree', 'workYearNum', 'personHonorTree', 'teacherTitleTree',
  'salary', 'complaint', 'areaAcronym', 'postWorkYearNum', 'yearOldNum', 'hotFlag']

extrs.map(function (name, index) {
  fs.writeFile('rawData/' + name + '.json', JSON.stringify(Dict[name]), function (err) {
    if (err) {
      console.info(err);
      throw err;
    }
    console.log('The file ' + name + ' has been saved!');
  });
})


//生成rawData/index.js
let importStr = '';
let exportStr = 'module.exports = {';
APIS.map(function (item, index) {
  importStr += "const " + item.name + " = require('../rawData/" + item.name + ".json');\n";
  exportStr += item.name + ', '
})


extrs.map(function (name) {
  importStr += "const " + name + " = require('../rawData/" + name + ".json');\n";
  exportStr += name + ', '
})

exportStr = exportStr.substring(-1, exportStr.length - 2)
exportStr += '}'

fs.writeFile('src/data.js', importStr + exportStr, function (err) {
  if (err) {
    console.info(err);
    throw err;
  }
  console.log('The file index.js has been saved!');
});