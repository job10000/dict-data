"use strict";

var companyType = require('../rawData/companyType.json');

var area = require('../rawData/area.json');

var hotCity = require('../rawData/hotCity.json');

var jobType = require('../rawData/jobType.json');

var personHonor = require('../rawData/personHonor.json');

var teacherTitle = require('../rawData/teacherTitle.json');

var companyVip = require('../rawData/companyVip.json');

var newsType = require('../rawData/newsType.json');

var complaintType = require('../rawData/complaintType.json');

var complaintReason = require('../rawData/complaintReason.json');

var teacherNumber = require('../rawData/teacherNumber.json');

var studentNumber = require('../rawData/studentNumber.json');

var workType = require('../rawData/workType.json');

var jobLevel = require('../rawData/jobLevel.json');

var prelectWay = require('../rawData/prelectWay.json');

var course = require('../rawData/course.json');

var language = require('../rawData/language.json');

var problemType = require('../rawData/problemType.json');

var userManualType = require('../rawData/userManualType.json');

var settlementMethod = require('../rawData/settlementMethod.json');

var workYear = require('../rawData/workYear.json');

var education = require('../rawData/education.json');

var companyInterviewResult = require('../rawData/companyInterviewResult.json');

var companySendType = require('../rawData/companySendType.json');

var teacherCert = require('../rawData/teacherCert.json');

var sex = require('../rawData/sex.json');

var location = require('../rawData/location.json');

var party = require('../rawData/party.json');

var marriage = require('../rawData/marriage.json');

var platform = require('../rawData/platform.json');

var intentionCompany = require('../rawData/intentionCompany.json');

var jobWanted = require('../rawData/jobWanted.json');

var arrivalTime = require('../rawData/arrivalTime.json');

var welfare = require('../rawData/welfare.json');

var resumeProcessType = require('../rawData/resumeProcessType.json');

var linkType = require('../rawData/linkType.json');

var validType = require('../rawData/validType.json');

var advertStatus = require('../rawData/advertStatus.json');

var jobStatus = require('../rawData/jobStatus.json');

var school = require('../rawData/school.json');

var talentRank = require('../rawData/talentRank.json');

var areaTree = require('../rawData/areaTree.json');

var jobTypeTree = require('../rawData/jobTypeTree.json');

var companyTypeTree = require('../rawData/companyTypeTree.json');

var workYearNum = require('../rawData/workYearNum.json');

var personHonorTree = require('../rawData/personHonorTree.json');

var teacherTitleTree = require('../rawData/teacherTitleTree.json');

var salary = require('../rawData/salary.json');

var complaint = require('../rawData/complaint.json');

var areaAcronym = require('../rawData/areaAcronym.json');

var postWorkYearNum = require('../rawData/postWorkYearNum.json');

var yearOldNum = require('../rawData/yearOldNum.json');

var hotFlag = require('../rawData/hotFlag.json');

module.exports = {
  companyType: companyType,
  area: area,
  hotCity: hotCity,
  jobType: jobType,
  personHonor: personHonor,
  teacherTitle: teacherTitle,
  companyVip: companyVip,
  newsType: newsType,
  complaintType: complaintType,
  complaintReason: complaintReason,
  teacherNumber: teacherNumber,
  studentNumber: studentNumber,
  workType: workType,
  jobLevel: jobLevel,
  prelectWay: prelectWay,
  course: course,
  language: language,
  problemType: problemType,
  userManualType: userManualType,
  settlementMethod: settlementMethod,
  workYear: workYear,
  education: education,
  companyInterviewResult: companyInterviewResult,
  companySendType: companySendType,
  teacherCert: teacherCert,
  sex: sex,
  location: location,
  party: party,
  marriage: marriage,
  platform: platform,
  intentionCompany: intentionCompany,
  jobWanted: jobWanted,
  arrivalTime: arrivalTime,
  welfare: welfare,
  resumeProcessType: resumeProcessType,
  linkType: linkType,
  validType: validType,
  advertStatus: advertStatus,
  jobStatus: jobStatus,
  school: school,
  talentRank: talentRank,
  areaTree: areaTree,
  jobTypeTree: jobTypeTree,
  companyTypeTree: companyTypeTree,
  workYearNum: workYearNum,
  personHonorTree: personHonorTree,
  teacherTitleTree: teacherTitleTree,
  salary: salary,
  complaint: complaint,
  areaAcronym: areaAcronym,
  postWorkYearNum: postWorkYearNum,
  yearOldNum: yearOldNum,
  hotFlag: hotFlag
};