"use strict";

var area = require('../rawData/area.json');

var hotCity = require('../rawData/hotCity.json');

var jobType = require('../rawData/jobType.json');

var areaAcronym = require('../rawData/areaAcronym.json');

var welfare = require('../rawData/welfare.json');

var education = require('../rawData/education.json');

var workYear = require('../rawData/workYear.json');

var workType = require('../rawData/workType.json');

var teacherNumber = require('../rawData/teacherNumber.json');

var studentNumber = require('../rawData/studentNumber.json');

var companyType = require('../rawData/companyType.json');

module.exports = {
  area: area,
  hotCity: hotCity,
  jobType: jobType,
  areaAcronym: areaAcronym,
  welfare: welfare,
  education: education,
  workYear: workYear,
  workType: workType,
  companyType: companyType,
  teacherNumber: teacherNumber,
  studentNumber: studentNumber
};