const area = require('../rawData/area.json');
const hotCity = require('../rawData/hotCity.json');
const jobType = require('../rawData/jobType.json');
const areaAcronym = require('../rawData/areaAcronym.json');
const welfare = require('../rawData/welfare.json');
const education = require('../rawData/education.json');
const workYear = require('../rawData/workYear.json');
const workType = require('../rawData/workType.json');
const teacherNumber = require('../rawData/teacherNumber.json');
const studentNumber = require('../rawData/studentNumber.json');
const companyType = require('../rawData/companyType.json');


module.exports = {area, hotCity, jobType,areaAcronym, welfare, education, workYear, workType, companyType, teacherNumber, studentNumber}