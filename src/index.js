import * as Dict from './data';
import {setDict} from './helper'
setDict(Dict);

export * from './helper'
export default Dict;
export { Dict};