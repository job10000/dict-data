import * as Dict from './data_front';
import {setDict} from './helper'
setDict(Dict);

export default Dict;
export { Dict };
export * from './helper'