let Dict;

export function setDict(dict) {
  Dict = dict;
}


/**
 * 通过值获取名称
 * @param type
 * @param value
 * @param isfullName
 * @returns {*}
 */
export function code2Name (type, value, isfullName = false) {
  let item = _find(Dict[type], {value: value});

  if (!item) {
    return '';
  }

  if (isfullName) return item.tn || item.sn;

  return item.label;
}
/**
 * 通过值获取地区名称
 * @param value
 */
export function code2AreaName (value) {
  return code2Name('area', value, true);
}

export function code2AreaFullName (value) {
  let item = _find(Dict['area'], {value: value});

  if (!item) {
    return '';
  }

  if (item.rn != item.pn) { //第三级时， 需添加第一集名称
    return item.rn + '-' +(item.tn || item.sn);
  }

  return item.tn || item.sn;
}

/**
 * 获取区间值名称
 * @param type
 * @param value
 * @param isfullName
 * @returns {*}
 */
export function code2RangeName (type, min, max) {

  let minItem = _find(Dict[type], {value: min});
  let maxItem = _find(Dict[type], {value: max});

  if(!minItem && !maxItem) {
    return ''
  }

  if (!minItem && maxItem) {
    return maxItem.label + '以下'
  }
  if (minItem && !maxItem) {
    return minItem.label + '以上'
  }

  return minItem.label + '-' + maxItem.label;
}

/**
 * 获取热门地区字典
 * @returns {*[]}
 */
export function getHotAreas () {
  return [{label: '广东', value: 440000,"p":"guangdong"},
    {label: '广州', value: 440100, p:'guangzhou'},
    {label: '东莞', value: 441900, p:'dongguan'},
    {label: '深圳', value: 440300, p:'shenzhen'},
    {label: '浙江', value: 330000, p:'zhejiang'},
    {label: '杭州', value: 330100, p:'hangzhou'},
    {label: '湖北', value: 420000, p:'hubei'},
    {label: '安徽', value: 340000, p:'anhui'},
    {label: '江苏', value: 320000, p:'jiangsu'},
    {label: '四川', value: 510000, p:'sichuan'},
    {label: '上海', value: 310000, p:'shanghai'},
    {label: '北京', value: 110000, p:'beijing'}
  ];
}
/**
 * 是否直辖市
 * @param value
 * @returns {boolean}
 */
export function isDirectArea (value) {
  return [110000, 120000, 310000, 500000].indexOf(value) > -1
}

/**
 * 获取第一级数据
 * @param type
 * @param withAll
 * @returns {Array}
 */
export function getTopData (type, withAll = false, exclude=[]) {
  return _filter(Dict[type], function (item) {
    if (!withAll) {
      return item.value != 0 && item.pc == 0 && !exclude.includes(item.value);
    }
    return item.pc == 0 && !exclude.includes(item.value);
  });
}
/**
 * 查找元素
 * @param type
 * @param value
 */
export function findItem (type, value) {
  return _find(Dict[type], {value: value});
}

/**
 * 通过名称查找元素
 * @param type
 * @param label
 */
export function findItemByLabel (type, label) {
  return _find(Dict[type], {label: label});
}

/**
 * 通过指定字段获取值
 * @param type
 * @param field
 * @param value
 * @param getField
 * @returns {null}
 */
export function getCodeByField (type, field, value, getField) {
  const data = Dict[type]
  const result = data.find(item => item[field] === value)
  return result ? result[getField] : null
}

/**
 * 获取子一级数据
 * @param type
 * @param value
 * @returns {Array}
 */
export function getChildData (type, value) {
  return _filter(Dict[type], {pc: value});
}

/**
 * 是否存在父级
 * @param item
 * @returns {boolean}
 */
export function hasParentItem (item) {
  return item.pc !== 0
}

/**
 * 是否存在子节点
 * @param type
 * @param value
 * @returns {boolean}
 */
export function hasChildData (type, value) {
  return !!_find(Dict[type], {pc: value});
}

/**
 * 获取Code路径
 * 如： [3300000, 330100, 330101]
 * @param type
 * @param value
 * @returns {*[]}
 */
export function getCodePath (type, value) {
  let path = [value];

  let item = getParentItem(type, value);
  if (item) {
    path.unshift(item.value)
    item = getParentItem(type, item.value);
    if (item) {
      path.unshift(item.value)
    }
  }

  return path;
}

/**
 * 判断是否是 属于包含关系
 * @param type
 * @param value1
 * @param value2
 * @returns {boolean}
 */
export function isImmediate (type, value1, value2) {
  let item1 = findItem(type, value1)
  let item2 = findItem(type, value2)

  if (!item1 || !item2) return false;

  return item1.value == item2.value || item1.pc == item2.value || item1.rc == item2.value ||
    item2.pc == item1.value || item2.rc == item1.value;

}

/**
 * 获取父元素
 * @param type
 * @param value
 * @returns {null}
 */
export function getParentItem (type, value) {
  let item = findItem(type, value);
  if (item && item.pc) {
    item = findItem(type, item.pc)
    return item;
  }
  return null;
}

/**
 * 获取工作年限字典
 * @param date
 * @returns {*}
 */
export function getWorkYears (date) {
  let gTime = new Date(date).getTime();
  const oneYear = 31536000000;
  let now = Date.now();
  if (gTime > now + oneYear) {
    return {
      label: '在读学生',
      value: -1
    }
  }

  if (gTime < now + oneYear && gTime > now - oneYear) {
    return {
      label: '应届毕业生',
      value: 0
    }
  }

  let years = Math.round((now - gTime) / oneYear)
  return {
    label: years + '年工作经验',
    value: years
  }
}

//HACK：IE Edge之前的版本不支持Array.find方法
if(!Array.prototype.find){
  Array.prototype.find = function(callback) {
    return callback && (this.filter(callback)|| [])[0];
  };
}

function _find(obj, param) {
  if (!obj) return null

  if (typeof param === 'function') {
    return Array.prototype.find.call(obj, param);
  }

  return Array.prototype.find.call(obj,function(item) {
    let keys = Object.keys(param);
    let result = keys.length > 0;

    keys.map(function (key) {
      if (item[key] != param[key]) {
        result = false
      }
    })

    return result
  })
}

function _filter(obj, param) {
  if (!obj) return null

  if (typeof param === 'function') {
    return Array.prototype.filter.call(obj, param);
  }

  return Array.prototype.filter.call(obj,function(item) {
    let keys = Object.keys(param);
    let result = keys.length > 0;

    keys.map(function (key) {
      if (item[key] != param[key]) {
        result = false
      }
    })

    return result
  })
}